﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.ExtendedModels
{
    public class PersonaExtended
    {
        [Key]
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Identificacion { get; set; }
        public string Estado { get; set; }
        public DateTime Registro { get; set; }
        public IEnumerable<Usuario> Usuarios { get; set; }

        public PersonaExtended()
        {
        }

        public PersonaExtended(Persona persona)
        {
            Id = persona.Id;
            Nombre = persona.Nombre;
            Apellido = persona.Apellido;
            Identificacion = persona.Identificacion;
            Estado = persona.Estado;
            Registro = persona.Registro;
            
        }
    }
}

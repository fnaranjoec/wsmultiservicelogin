﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.ExtendedModels
{
    public class CategoriaExtended
    {
        [Key]
        public string Id { get; set; }
        public string PadreId { get; set; }
        public string Nombre { get; set; }
        public string Foto { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public DateTime Registro { get; set; }
        public IEnumerable<Categoria> SubCategorias { get; set; }

        public CategoriaExtended()
        {
        }

        public CategoriaExtended(Categoria categoria)
        {
            Id = categoria.Id;
            PadreId = categoria.PadreId;
            Nombre = categoria.Nombre;
            Foto = categoria.Foto;
            Descripcion = categoria.Descripcion;
            Estado = categoria.Estado;
            Registro = categoria.Registro;
        }

    }
}

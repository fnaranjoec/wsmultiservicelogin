﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class PersonaExtensions
    {
        public static void Map(this Persona dbPersona, Persona persona)
        {
        dbPersona.Nombre = persona.Nombre;
        dbPersona.Apellido = persona.Apellido;
        dbPersona.Identificacion = persona.Identificacion;
        dbPersona.Estado = persona.Estado;
        dbPersona.Registro = persona.Registro;
        }
    }
}

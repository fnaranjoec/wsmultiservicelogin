﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class CategoriaExtensions
    {
        public static void Map(this Categoria dbCategoria, Categoria categoria)
        {
            dbCategoria.PadreId= categoria.PadreId;
            dbCategoria.Nombre = categoria.Nombre;
            dbCategoria.Descripcion = categoria.Descripcion;
            dbCategoria.Foto = categoria.Foto;
            dbCategoria.Estado = categoria.Estado;
            dbCategoria.Registro = categoria.Registro;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Text;
using Entities.ExtendedModels;
using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Entities
{
    public partial class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions<RepositoryContext> options)
            : base(options)
        {
        }

        //Defino los DataSets con sus gets y sets
        public virtual DbSet<Persona> Personas { get; set; }
        public virtual DbSet<PersonaLogin> PersonasLogin { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<Categoria> Categorias { get; set; }

        //Si no esta definida la conexion en el appsettings.json la defino aqui.
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql("server=localhost;port=3306;userid=root;password=Gjc*27911972;database=multiservice;");
            }
        }

    }
}

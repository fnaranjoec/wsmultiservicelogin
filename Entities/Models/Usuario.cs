﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("tblUsuario")]
    public class Usuario : IEntity
    {
        [Key]
        [Column("CIdUsuario")]
        [StringLength(36, ErrorMessage = "El id usuario no puede ser mas de 36 caracteres")]
        public string Id { get; set; }

        [Column("CIdPersona")]
        [StringLength(36, ErrorMessage = "El id persona no puede ser mas de 36 caracteres")]
        public string PersonaId { get; set; }

        [Column("CNoUsuario")]
        [Required(ErrorMessage = "Nombre usuario es requerido")]
        [StringLength(20, ErrorMessage = "El nombre de usuario no puede ser mas de 20 caracteres")]
        public string Alias { get; set; }

        [Column("CTxClave")]
        [Required(ErrorMessage = "Clave es requerido")]
        [StringLength(10, ErrorMessage = "La clave no puede ser mas de 10 caracteres")]
        public string Clave { get; set; }

        [Column("CNuMovil")]
        [Required(ErrorMessage = "Movil es requerido")]
        [StringLength(10, ErrorMessage = "El número de movil no puede ser mas de 10 caracteres")]
        public string Movil { get; set; }

        [Column("CTxCorreo")]
        [Required(ErrorMessage = "Correo es requerido")]
        [StringLength(255, ErrorMessage = "El correo no puede ser mas de 255 caracteres")]
        public string Correo { get; set; }


        //[Required(ErrorMessage = "Estado es requerido")]
        [Column("CCeUsuario")]
        [StringLength(1, ErrorMessage = "El estado no puede ser mas de 1 caracteres")]
        public string Estado { get; set; }

        //[Required(ErrorMessage = "Fecha de registro es requerido")]
        [Column("DFxRegistro")]
        public DateTime Registro { get; set; }

    }
}

﻿using Entities.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Objects;
using System.Text;

namespace Entities.Models
{
    [Table("tblPersona")]
    public class Persona : IEntity
    {
        [Key]
        [Column("CIdPersona")]
        [StringLength(36, ErrorMessage = "El id persona no puede ser mas de 36 caracteres")]
        public string Id { get; set; }

        [Column("CNoPersona")]
        [Required(ErrorMessage = "Nombre es requerido")]
        [StringLength(100, ErrorMessage = "El nombre no puede ser mas de 100 caracteres")]
        public string Nombre { get; set; }

        [Column("CApPersona")]
        [Required(ErrorMessage = "Apellido es requerido")]
        [StringLength(100, ErrorMessage = "El apellido no puede ser mas de 100 caracteres")]
        public string Apellido { get; set; }

        [Column("CNuIdentificacion")]
        [Required(ErrorMessage = "Identificación es requerido")]
        [StringLength(10, ErrorMessage = "El número de identificación no puede ser mas de 10 caracteres")]
        public string Identificacion { get; set; }

        //[Required(ErrorMessage = "Estado es requerido")]
        [Column("CCePersona")]
        [StringLength(1, ErrorMessage = "El estado no puede ser mas de 1 caracteres")]
        public string Estado { get; set; }

        //[Required(ErrorMessage = "Fecha de registro es requerido")]
        [Column("DFxRegistro")]
        public DateTime Registro { get; set; }

        public static explicit operator Persona(ObjectResult<Persona> v)
        {
            throw new NotImplementedException();
        }
    }
}

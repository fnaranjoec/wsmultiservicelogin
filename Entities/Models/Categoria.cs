﻿using Entities.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("tblCategoria")]
    public class Categoria : IEntity
    {
        [Key]
        [Column("CIdCategoria")]
        [StringLength(36, ErrorMessage = "El id categoria no puede ser mas de 36 caracteres")]
        public string Id { get; set; }

        [Column("CIdCatPadre")]
        [StringLength(36, ErrorMessage = "El id categoria padre no puede ser mas de 36 caracteres")]
        public string PadreId { get; set; }

        [Column("CNoCategoria")]
        [Required(ErrorMessage = "Nombre es requerido")]
        [StringLength(100, ErrorMessage = "El nombre de categoria no puede ser mas de 100 caracteres")]
        public string Nombre { get; set; }

        [Column("CTxUrlFoto")]
        [Required(ErrorMessage = "Foto es requerido")]
        [StringLength(500, ErrorMessage = "La URL de la foto no puede ser mas de 500 caracteres")]
        public string Foto { get; set; }

        [Column("CDsCategoria")]
        [Required(ErrorMessage = "Descripción es requerido")]
        [StringLength(255, ErrorMessage = "La descripcion no puede ser mas de 255 caracteres")]
        public string Descripcion { get; set; }

        //[Required(ErrorMessage = "Estado es requerido")]
        [Column("CCeCategoria")]
        [StringLength(1, ErrorMessage = "El estado no puede ser mas de 1 caracteres")]
        public string Estado { get; set; }

        //[Required(ErrorMessage = "Fecha de registro es requerido")]
        [Column("DFxRegistro")]
        public DateTime Registro { get; set; }



    }
}

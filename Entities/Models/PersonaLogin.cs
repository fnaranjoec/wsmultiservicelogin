﻿using Entities.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Objects;
using System.Text;

namespace Entities.Models
{
    [Table("vw_personalogin")]
    public class PersonaLogin : IEntity
    {
        [Key]
        [Column("CIdPersona")]
        [StringLength(36, ErrorMessage = "El id persona no puede ser mas de 36 caracteres")]
        public string Id { get; set; }

        [Column("CNoPersona")]
        [Required(ErrorMessage = "Nombre es requerido")]
        [StringLength(100, ErrorMessage = "El nombre no puede ser mas de 100 caracteres")]
        public string Nombre { get; set; }

        [Column("CApPersona")]
        [Required(ErrorMessage = "Apellido es requerido")]
        [StringLength(100, ErrorMessage = "El apellido no puede ser mas de 100 caracteres")]
        public string Apellido { get; set; }

        [Column("CNuIdentificacion")]
        [Required(ErrorMessage = "Identificación es requerido")]
        [StringLength(10, ErrorMessage = "El número de identificación no puede ser mas de 10 caracteres")]
        public string Identificacion { get; set; }

        //[Required(ErrorMessage = "Estado es requerido")]
        [Column("CCePersona")]
        [StringLength(1, ErrorMessage = "El estado no puede ser mas de 1 caracteres")]
        public string Estado { get; set; }

        //[Required(ErrorMessage = "Fecha de registro es requerido")]
        [Column("DFxRegistro")]
        public DateTime Registro { get; set; }

        [Column("CIdUsuario")]
        [StringLength(36, ErrorMessage = "El id de usuario no puede ser mas de 36 caracteres")]
        public string UsuarioId { get; set; }

        [Column("CNoUsuario")]
        [StringLength(20, ErrorMessage = "El nombre de usuario no puede ser mas de 20 caracteres")]
        public string Usuario { get; set; }

        [Column("CNuMovil")]
        [StringLength(10, ErrorMessage = "El movil no puede ser mas de 10 caracteres")]
        public string Movil { get; set; }

        [Column("CTxCorreo")]
        [StringLength(255, ErrorMessage = "El correo no puede ser mas de 255 caracteres")]
        public string Correo { get; set; }

        [Column("CTxClave")]
        [StringLength(10, ErrorMessage = "La clave no puede ser mas de 10 caracteres")]
        public string Clave { get; set; }


    }
}

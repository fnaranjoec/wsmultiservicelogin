﻿using Contracts;
using Entities;
using Entities.ExtendedModels;
using Entities.Extensions;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;

namespace Repository
{
    public class PersonaRepository : RepositoryBase<Persona>, IPersonaRepository
    {
        public PersonaRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public IEnumerable<Persona> GetAllPersonas()
        {
            return FindAll()
                .OrderBy(ps => ps.Apellido)
                .DefaultIfEmpty(new Persona());
        }


        /*
        public IEnumerable<Persona> ExecStoreProcedure (string query, params object[] parameters)
        {
            return ExecWithStoreProcedure(query, parameters);
        }
        */


        public Persona GetPersonaById(string personaId)
        {
            return FindByCondition(ps => ps.Id.Equals(personaId))
                    .DefaultIfEmpty(new Persona())
                    .FirstOrDefault();
        }

        public Persona GetPersonaByIdentificacion(string identificacion)
        {
            return FindByCondition(ps => ps.Identificacion.Equals(identificacion))
                    .DefaultIfEmpty(new Persona())
                    .FirstOrDefault();
        }

        /*
        public PersonaExtended GetPersonaWithDetails(string personaId)
        {
            return new PersonaExtended(GetPersonaById(personaId))
            {
                Usuarios = RepositoryContext.Usuarios
                    .Where(u => u.PersonaId == personaId)
            };
        }

        public PersonaExtended LoginByIdentificacion(string identificacion, string clave)
        {
            Persona persona = GetPersonaByIdentificacion(identificacion);
            if (!String.IsNullOrEmpty(persona.Id))
            {
                return new PersonaExtended(persona)
                {
                    Usuarios = RepositoryContext.Usuarios
                        .Where(u => u.PersonaId == persona.Id && u.Clave == clave)
                };
            }
            else {
                return new PersonaExtended();
            }

        }
        */

        public void CreatePersona(Persona persona)
        {
            //Asigno nuego guid antes enviar a la base de datos el objeto persona
            persona.Id = Guid.NewGuid().ToString();
            Create(persona);
            Save();
        }

        
        public void UpdatePersona(Persona dbPersona, Persona persona)
        {
            dbPersona.Map(persona);
            Update(dbPersona);
            Save();
        }


    }
}

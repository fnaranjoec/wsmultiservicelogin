﻿using Contracts;
using Entities;
using Entities.ExtendedModels;
using Entities.Extensions;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;

namespace Repository
{
    public class PersonaLoginRepository : RepositoryBase<PersonaLogin>, IPersonaLoginRepository
    {
        public PersonaLoginRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public IEnumerable<PersonaLogin> ExecStoreProcedure (string query, params object[] parameters)
        {
            return ExecWithStoreProcedure(query, parameters);
        }


    }
}

﻿using Contracts;
using Entities;
using Entities.ExtendedModels;
using Entities.Extensions;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

namespace Repository
{
    public class CategoriaRepository : RepositoryBase<Categoria>, ICategoriaRepository
    {
        public CategoriaRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public IEnumerable<Categoria> GetAllCategorias()
        {
            return FindAll()
                .OrderBy(cat => cat.PadreId)
                .DefaultIfEmpty(new Categoria())
                .OrderBy(cat => cat.Id)
                .Where(cat => cat.PadreId==(GetCategoriaRoot().Id));
        }

        public Categoria GetCategoriaRoot()
        {
            return RepositoryContext.Categorias
                    .Where(subcat => subcat.PadreId == null)
                    .DefaultIfEmpty(new Categoria())
                    .FirstOrDefault(); 
        }

        public Categoria GetCategoriaById(string categoriaId)
        {
            return FindByCondition(cat => cat.Id.Equals(categoriaId))
                    .DefaultIfEmpty(new Categoria())
                    .FirstOrDefault();
        }



        public CategoriaExtended GetCategoriaWithDetails(string categoriaId)
        {

            return new CategoriaExtended(GetCategoriaById(categoriaId))
            {
                SubCategorias = RepositoryContext.Categorias
                    .Where(subcat => subcat.PadreId == categoriaId)
        };

        }
        


        public void CreateCategoria(Categoria categoria)
        {
            //Asigno nuego guid antes enviar a la base de datos el objeto persona
            categoria.Id = Guid.NewGuid().ToString();
            Create(categoria);
            Save();
        }

        
        public void UpdateCategoria(Categoria dbCategoria, Categoria categoria)
        {
            dbCategoria.Map(categoria);
            Update(dbCategoria);
            Save();
        }


    }
}

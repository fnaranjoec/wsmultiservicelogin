﻿using Contracts;
using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repoContext;
        private IPersonaRepository _persona;
        private IPersonaLoginRepository _personaLogin;
        private IUsuarioRepository _usuario;

        private ICategoriaRepository _categoria;

        public IPersonaRepository Persona
        {
            get
            {
                if (_persona == null)
                {
                    _persona = new PersonaRepository(_repoContext);
                }

                return _persona;
            }
        }

        public IPersonaLoginRepository PersonaLogin
        {
            get
            {
                if (_personaLogin == null)
                {
                    _personaLogin = new PersonaLoginRepository(_repoContext);
                }

                return _personaLogin;
            }
        }

        public IUsuarioRepository Usuario
        {
            get
            {
                if (_usuario == null)
                {
                    _usuario = new UsuarioRepository(_repoContext);
                }

                return _usuario;
            }
        }

        public ICategoriaRepository Categoria
        {
            get
            {
                if (_categoria == null)
                {
                    _categoria = new CategoriaRepository(_repoContext);
                }

                return _categoria;
            }
        }

        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }

    }
}

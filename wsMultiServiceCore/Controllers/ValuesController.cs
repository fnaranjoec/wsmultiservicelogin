﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace wsMultiServiceCore.Controllers
{



    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        private ILoggerManager _logger;
        private IRepositoryWrapper _repoWrapper;
        private IPersonaRepository _repository;

        public ValuesController(ILoggerManager logger, IRepositoryWrapper repoWrapper)
        {
            _logger = logger;
            _repoWrapper = repoWrapper;

        }



        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Persona>> Get()
        {

            /*
            _logger.LogInfo("Here is info message from our values controller.");
            _logger.LogDebug("Here is debug message from our values controller.");
            _logger.LogWarn("Here is warn message from our values controller.");
            _logger.LogError("Here is error message from our values controller.");
            */

            _logger.LogInfo("Obteniendo todas las personas");

            //var activosUsuarios = _repoWrapper.Usuario.FindByCondition(x => x.CCeUsuario.Equals("A"));
            var personas = _repoWrapper.Persona.FindAll();
            //var personas= _repoWrapper.Persona.FindByCondition(x => x.CCePersona.Equals("A"));

            return personas.ToList<Persona>();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<Persona>> Get(string id)
        {
            _logger.LogInfo("Obteniendo una personas");

            var personas= _repoWrapper.Persona.FindByCondition(x => x.Id.Equals(id));

            return personas.ToList<Persona>();
        }

        //string cidpersona, string cnopersona, string cappersona, string cnuidentificacion
        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Persona persona)
        {
            try
            {
                if (persona == null)
                {
                    _logger.LogError("Objeto Persona  enviado desde el cliente es nulo.");
                    return BadRequest("Objeto Persona es nula.");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Objeto Persona  enviado desde el cliente es nulo.");
                    return BadRequest("Modelo del objeto es invalido.");
                }

                persona.Estado = "A";
                persona.Registro = DateTime.Now;

                _repoWrapper.Persona.Create(persona);
                _repoWrapper.Persona.Save();
                return Ok(persona);

                //return CreatedAtRoute("OwnerById", new { id = persona.CIdPersona }, persona);
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo ocurrio dentro de la accion Create: {ex.Message}" + $"\r\n {ex.InnerException}");
                return StatusCode(500, "Error interno del servidor");
            }


        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


    }
}

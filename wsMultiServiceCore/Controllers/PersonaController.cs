﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace wsMultiServiceCore.Controllers
{
    [Route("api/persona")]
    public class PersonaController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public PersonaController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        public IActionResult GetAllPersonas()
        {
            try
            {
                var personas = _repository.Persona.GetAllPersonas();

                _logger.LogInfo($"Retornado todas las personas de la base de datos.");

                return Ok(personas);
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo salio mal en la acción GetAllPersonas: {ex.Message}, \r\n{ex.InnerException}");
                return StatusCode(500, "Error interno del servidor, revise el log.");
            }
        }


        [HttpGet("{id}", Name = "GetPersonaById")]
        public IActionResult GetPersonaById(string id)
        {
            try
            {
                var persona = _repository.Persona.GetPersonaById(id);

                if (String.IsNullOrEmpty(persona.Id)) 
                {
                    _logger.LogError($"Persona con id: {id}, no ha sido encotrada en la base de datos.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Retornada persona con id: {id}");
                    return Ok(persona);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo salio mal con acción GetPersonaById: {ex.Message}" + $"\r\n {ex.InnerException}");
                return StatusCode(500, "Error interno del servidor.");
            }
        }

        /*
        [HttpGet("{id}/usuario")]
        public IActionResult GetPersonaWithDetails(string id)
        {
            try
            {
                var personaExtended = _repository.Persona.GetPersonaWithDetails(id);

                if (String.IsNullOrEmpty(personaExtended.Id))
                {
                    _logger.LogError($"Persona con id: {id}, no ha sido encotrada en la base de datos.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Retornada persona y detalles con id: {id}");
                    return Ok(personaExtended);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo salio mal con acción GetPersonaWithDetails: {ex.Message}" + $"\r\n {ex.InnerException}");
                return StatusCode(500, "Error interno del servidor.");
            }
        }
        */


        [HttpPost("login")]
        public IActionResult LoginByIdentificacion([FromBody] Login login)
        {
            try
            {

                var persona = _repository.PersonaLogin.ExecWithStoreProcedure("call sps_UsuarioLogin(?, ?, ?)",
                                new MySql.Data.MySqlClient.MySqlParameter("cIdentificacion", login.Identificacion),
                                new MySql.Data.MySqlClient.MySqlParameter("cClave", login.Clave),
                                new MySql.Data.MySqlClient.MySqlParameter("cTipoLogin", "I")
                                );

                if (persona.Count() == 0)  
                {
                    _logger.LogError($"Persona con identificacion: {login.Identificacion}, no ha sido encotrada en la base de datos.");
                    return BadRequest("Objeto Persona es invalido: Identificacion o clave incorrectos.");
                }
                else
                {
                    _logger.LogInfo($"Retornada persona y detalles con id: {login.Identificacion}");
                    persona.First().Clave = "**********";
                    return Ok(persona);  
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo salio mal con acción LoginByIdentificacion: {ex.Message}" + $"\r\n {ex.InnerException}");
                return StatusCode(500, "Error interno del servidor.");
            }
        }


        [HttpPost]
        public IActionResult crear([FromBody] Persona persona)
        {
            try
            {
                if (persona == null)
                {
                    _logger.LogError("El objeto Persona enviado desde el cliente es nulo.");
                    return BadRequest("Objeto Persona es nulo");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("El objeto Persona enviado desde el cliente es invalido.");
                    return BadRequest("Modelo de objeto invalido");
                }

                _repository.Persona.CreatePersona(persona);

                return CreatedAtRoute("GetPersonaById", new { id = persona.Id });
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo salio mal con acción Persona crear: {ex.Message}" + $"\r\n {ex.InnerException}");
                return StatusCode(500, "Error interno del servidor.");
            }
        }


        [HttpPut("{id}")]
        public IActionResult actualizar(string id, [FromBody]Persona persona)
        {
            try
            {
                if (String.IsNullOrEmpty(persona.Id))
                {
                    _logger.LogError("El objeto Persona enviado desde el cliente es nulo.");
                    return BadRequest("Objeto Persona es nulo");

                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("El objeto Persona enviado desde el cliente es invalido.");
                    return BadRequest("Modelo de objeto invalido");
                }

                var dbPersona = _repository.Persona.GetPersonaById(id);
                if (String.IsNullOrEmpty(dbPersona.Id))
                {
                    _logger.LogError($"Persona con id: {id}, no ha sido encotrada en la base de datos.");
                    return NotFound();
                }

                _repository.Persona.UpdatePersona(dbPersona, persona);

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo salio mal con acción Persona actualizar: {ex.Message}" + $"\r\n {ex.InnerException}");
                return StatusCode(500, "Error interno del servidor.");

            }
        }

    }
}

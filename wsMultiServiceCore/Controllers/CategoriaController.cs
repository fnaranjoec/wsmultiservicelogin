﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities;
using Entities.ExtendedModels;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace wsMultiServiceCore.Controllers
{
    [Route("api/categoria")]
    public class CategoriaController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public CategoriaController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        public IActionResult GetAllCategorias()
        {
            try
            {
                var categorias = _repository.Categoria.GetAllCategorias();

                _logger.LogInfo($"Retornado todas las categorias de la base de datos.");

                return Ok(categorias);
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo salio mal en la acción GetAllCategorias: {ex.Message}, \r\n{ex.InnerException}");
                return StatusCode(500, "Error interno del servidor, revise el log.");
            }
        }


        [HttpGet("{id}", Name = "GetCategoriaById")]
        public IActionResult GetCategoriaById(string id)
        {
            try
            {
                var categoria = _repository.Categoria.GetCategoriaById(id);

                if (String.IsNullOrEmpty(categoria.Id)) 
                {
                    _logger.LogError($"Categoria con id: {id}, no ha sido encotrada en la base de datos.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Retornada categoria con id: {id}");
                    return Ok(categoria);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo salio mal con acción GetCategoriaById: {ex.Message}" + $"\r\n {ex.InnerException}");
                return StatusCode(500, "Error interno del servidor.");
            }
        }

        [HttpGet("{id}/subcat")]
        public IActionResult GetCategoriaWithDetails(string id)
        {
            try
            {
                var categoria = _repository.Categoria.GetCategoriaWithDetails(id);

                if (String.IsNullOrEmpty(categoria.Id))
                {
                    _logger.LogError($"Categoria con id: {id}, no ha sido encotrada en la base de datos.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Retornada categoria y detalles con id: {id}");
                    return Ok(categoria);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo salio mal con acción GetCategoriaWithDetails: {ex.Message}" + $"\r\n {ex.InnerException}");
                return StatusCode(500, "Error interno del servidor.");
            }
        }


        [HttpPut("{id}")]
        public IActionResult actualizar(string id, [FromBody]Categoria categoria)
        {
            try
            {
                if (String.IsNullOrEmpty(categoria.Id))
                {
                    _logger.LogError("El objeto Categoria enviado desde el cliente es nulo.");
                    return BadRequest("Objeto Categoria es nulo");

                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("El objeto Categoria enviado desde el cliente es invalido.");
                    return BadRequest("Modelo de objeto invalido");
                }

                var dbCategoria = _repository.Categoria.GetCategoriaById(id);
                if (String.IsNullOrEmpty(dbCategoria.Id))
                {
                    _logger.LogError($"Persona con id: {id}, no ha sido encotrada en la base de datos.");
                    return NotFound();
                }

                _repository.Categoria.UpdateCategoria(dbCategoria, categoria);

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"\r\nAlgo salio mal con acción Categoria actualizar: {ex.Message}" + $"\r\n {ex.InnerException}");
                return StatusCode(500, "Error interno del servidor.");

            }
        }

    }
}

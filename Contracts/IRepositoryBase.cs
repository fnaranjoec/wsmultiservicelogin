﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;
using System.Text;

namespace Contracts
{
    public interface IRepositoryBase<T>
    {
        IEnumerable<T> FindAll();
        IEnumerable<T> FindByCondition(Expression<Func<T, bool>> expression);
        IEnumerable<T> ExecWithStoreProcedure(string query, params object[] parameters);

        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Save();

    }
}

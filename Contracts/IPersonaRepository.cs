﻿using Entities;
using Entities.ExtendedModels;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Text;

namespace Contracts
{
    public interface IPersonaRepository : IRepositoryBase<Persona>
    {
        IEnumerable<Persona> GetAllPersonas();
        //IEnumerable<Persona> ExecStoreProcedure(string query, params object[] parameters);
        Persona GetPersonaById(string personaId);
        Persona GetPersonaByIdentificacion(string identificacion);
        //PersonaExtended GetPersonaWithDetails(string personaId);
        //PersonaExtended LoginByIdentificacion(string identificacion, string clave);
        void CreatePersona(Persona persona);
        void UpdatePersona(Persona dbpersona, Persona persona);
    }

}

﻿using Entities;
using Entities.ExtendedModels;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Text;

namespace Contracts
{
    public interface ICategoriaRepository : IRepositoryBase<Categoria>
    {
        IEnumerable<Categoria> GetAllCategorias();
        Categoria GetCategoriaById(string categoriaId);
        Categoria GetCategoriaRoot();
        CategoriaExtended GetCategoriaWithDetails(string categoriaId);
        void CreateCategoria(Categoria categoria);
        void UpdateCategoria(Categoria dbcategoria, Categoria categoria);

        


    }

}

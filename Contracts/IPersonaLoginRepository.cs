﻿using Entities;
using Entities.ExtendedModels;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Text;

namespace Contracts
{
    public interface IPersonaLoginRepository : IRepositoryBase<PersonaLogin>
    {
        IEnumerable<PersonaLogin> ExecStoreProcedure(string query, params object[] parameters);
    }

}

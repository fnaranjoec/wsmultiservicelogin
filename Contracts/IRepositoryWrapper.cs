﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IRepositoryWrapper
    {
        IPersonaRepository Persona { get; }
        IPersonaLoginRepository PersonaLogin { get; }
        IUsuarioRepository Usuario { get; }
        ICategoriaRepository Categoria { get; }
    }
}
